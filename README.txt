=====================================
Outlook Linker (PopClip Ext) - 1.0.0
=====================================

Author: James Gibbard (http://jgibbard.me.uk)
Website: http://jgibbard.me.uk/bitbucket/outlook-linker-popclip-extension

Outlook Linker (PopClip Ext) allows you to quickly reconise and open 
Outlook Linker URLs using PopClip. This helps strealine the opening of 
Outlook messages when an appliation doesn't recongisise the custom URL format.

Installation:
--------------
Note: As this PopClip Extention is not yet registered with PopClip, 
you will get prompted to install this unsigned extention. 

1. Download the PopCli Extention (Outlook linker - http://bit.ly/1fLYJ7M)
2. Install the service extenion.


Usage:
-------
1. Highlist an Outlook Linker custom URL.
2. Click on the Outlook Linker icon on the PopClip menu.
3. Watch your selected message open in Outlook - and smile :) 